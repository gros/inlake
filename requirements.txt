fastapi==0.83.0
starlette>=0.19.1
uvicorn>=0.15.0,<0.16.0
# boto3>=1.24.67
# botocore>=1.27.67
aiobotocore>=2.4.0
requests>=2.7.0
confluent-kafka<=1.9.2
python-multipart<=0.0.5
fastavro>=1.7.0
aiokafka>=0.7.2
python-dotenv==1.0.0