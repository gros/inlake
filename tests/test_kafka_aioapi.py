from inlake.kafka_aioapi import AIOKafkaApi
from pathlib import Path
import configparser
import pytest
from uuid import uuid4
import pytest_asyncio
import os
from datetime import datetime


WORKDIR                 = Path(__file__).parent.resolve()
CONFIG_PATH             = Path(WORKDIR, './_test_config_inlake.cfg')


@pytest_asyncio.fixture
async def aio_kafka_api():
    config = configparser.ConfigParser()
    try:
        with open(CONFIG_PATH) as f:
            config.read_file(f)
    except IOError:
        raise FileNotFoundError("config.cfg file not found at CONFIG_PATH={}".format(CONFIG_PATH))
    
    aio_kapi2 = AIOKafkaApi()
    
    await aio_kapi2.connect_and_start(basicconfig=dict(config['kafka']), aioconfig=dict(config['kafka-aio']))

    # await aio_kapi.start()
    yield aio_kapi2
    await aio_kapi2.stop()


@pytest.mark.usefixtures("aio_kafka_api")
class TestAIOKafkaAPI():
    __test__ = False
    
    # Admin
    ##############################
    @pytest.mark.asyncio
    async def test_healthcheck(self, aio_kafka_api):
        assert await aio_kafka_api.healthcheck() is True

    def test_topic_creation(self, aio_kafka_api):
        test_topic_name = "abasicapi_test_topic+"+ str(uuid4())
        aio_kafka_api.create_topic(topic_name=test_topic_name)
        # assert (test_topic_name in aio_kafka_api.adminclient.list_topics(test_topic_name).topics) is True
        assert (test_topic_name in aio_kafka_api.adminclient.list_topics(test_topic_name).topics) is True
        print(aio_kafka_api.adminclient.list_topics().topics)
        aio_kafka_api.adminclient.delete_topics([test_topic_name])

    # Producer
    ##############################
    @pytest.mark.asyncio
    async def test_produce_message(self, aio_kafka_api):
        dtnow = f'{datetime.now():%Y-%m-%d--%H-%M-%S%z}'
        ttopic_name = f"test_topiic{dtnow}" # + str(uuid4())
        tkey = "demo-key".encode('utf-8')
        tvalue = f"demo-value+{dtnow}".encode('utf-8')

        aio_kafka_api.create_topic(topic_name=ttopic_name)
        await aio_kafka_api.produce_message(topic=ttopic_name, key=tkey, value=tvalue)

        # aio_kafka_api.adminclient.delete_topics([ttopic_name])

        # TODO:assertion test with consumer
        # aio_kafka_api.adminclient.delete_topics([ttopic_name])



    # def test_produce_message(self, basic_kafka_api):
    #     test_topic_name = "abasicapi_messageprod_topic"# + str(uuid4())
    #     basic_kafka_api.create_topic(topic_name=test_topic_name)

    #     basic_kafka_api.produce_message(topic=test_topic_name, key="clefmsg", value="valuemsg")

    #     basic_kafka_api.adminclient.delete_topics([test_topic_name])
