from inlake.schemaregistry_basicapi import InlkSchemaRClient
from confluent_kafka.schema_registry.schema_registry_client import Schema, SchemaReference, SchemaRegistryClient
from uuid import uuid4

dummy_good_avro_raw = """
        {
            "type": "record",
            "name": "dummy_user",
            "namespace": "org.test.dummyuser",
            "fields": [
                {
                "name": "firstname",
                "type": "string"
                },
                {
                "name": "lastname",
                "type": "string"
                },
                {
                "name": "job",
                "type": "string"
                }
            ]
        }
    """

class TestInlkSchemaRClient:
    __test__ = True
    
    def test_offline_data_validation(self):
        dummy_schema_name = 'test_schema+' + str(uuid4())

        my_schema = Schema(  schema_str=dummy_good_avro_raw,
                             schema_type='AVRO')
        
        my_stone_client = InlkSchemaRClient()

        good_dummy_record = {"firstname": "Bob", "lastname": "Du Buc Du Ferray", "job": "Pianiste"}
        validated = my_stone_client.validate_data_against_schema(schema=my_schema, input_data=good_dummy_record)
        
        # myschema.validate_data_against_schema(input_data=good_dummy_record)
        assert validated is True
        