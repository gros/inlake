from inlake.s3_basicapi import AIOs3Api

from pathlib import Path
import configparser
import pytest

import pytest_asyncio
import os
from datetime import datetime

WORKDIR                 = Path(__file__).parent.resolve()
CONFIG_PATH             = Path(WORKDIR, './_test_config_inlake.cfg')

@pytest_asyncio.fixture
async def aio_s3_api():
    config = configparser.ConfigParser()
    try:
        with open(CONFIG_PATH) as f:
            config.read_file(f)
    except IOError:
        raise FileNotFoundError("config.cfg file not found at CONFIG_PATH={}".format(CONFIG_PATH))
    
    aio_sapi = AIOs3Api()
    
    await aio_sapi.connect(dict(config['s3-internal']))
    yield aio_sapi
    await aio_sapi.stop()


@pytest.mark.usefixtures("aio_s3_api")
class TestAIOKafkaAPI():
    __test__ = False


    # Bucket List
    ##############################
    @pytest.mark.asyncio
    async def test_produce_message(self, aio_s3_api):
        await aio_s3_api.aio_listbuckets()
        print('ok')
        assert False