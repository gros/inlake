import os
from app.main import app
import pytest
from fastapi.testclient import TestClient
import json
from uuid import uuid4
import asyncio

from inlake.kafka_aioapi import AIOKafkaApi
from aiobotocore.session import get_session
from urllib.parse import urlparse

from dotenv import load_dotenv
load_dotenv()

client = TestClient(app)

@pytest.mark.skip(reason="wip")
def test_unsecured_record_json():
    with TestClient(app) as client:
        schema = "test_schema_j"
        inlkey = schema
        good_record = {"firstname": "Bob", "lastname": "Du Buc Du Ferray", "job": "Pianiste"}
        
        response = client.post(
            "/ingress/unsecured/record-json",
            json={"inlk_key": f"{inlkey}", 
                  "content": f"{json.dumps(good_record)}",
                  "topic_override": "inlake-test"}
        )
        
        print(response.json())
        assert response.status_code == 201

async def delete_file_from_s3(bucket: str, key: str):
    session = get_session()
    async with session.create_client(   's3',
                                        aws_access_key_id=os.environ['MINIO_ROOT_USER'],
                                        aws_secret_access_key=os.environ['MINIO_ROOT_PASSWORD'],
                                        endpoint_url=os.environ['MINIO_HOST']
                                        ) as client:
        e = await client.delete_object(Bucket=bucket, Key=key)
    return e

def test_unsecured_record_json_bin():
    with TestClient(app) as client:
        schema = "test_schema_jb"
        s3_symbolic = "minio://astragale-testbucket/data.bin"
        inlkey = schema
        data = b'\x01'*2048
        good_record = {"firstname": "Bob", "lastname": "Du Buc Du Ferray", "job": "Pianiste", "__resource_path__": s3_symbolic}
        
        response = client.post(
            "/ingress/unsecured/record-json-and-binary",
            params={"inlk_key": f"{inlkey}", 
                  "content": f"{json.dumps(good_record)}",
                  "topic_override": "inlake-test"},
            files={'file': ('filetitle', data)}
        )
        
        print(response.json())
        assert response.status_code == 201

        # clean
        o = urlparse(s3_symbolic)
        asyncio.run(delete_file_from_s3(bucket=o.netloc, key=o.path))

def test_unsecured_record_json_BADbucket_bin():
    with TestClient(app) as client:
        schema = "test_schema_jb"
        s3_symbolic = "minio://astragale-nobucket/data.bin"
        inlkey = schema
        data = b'\x01'*2048
        good_record = {"firstname": "Bob", "lastname": "Du Buc Du Ferray", "job": "Pianiste", "__resource_path__": s3_symbolic}
        
        response = client.post(
            "/ingress/unsecured/record-json-and-binary",
            params={"inlk_key": f"{inlkey}", 
                  "content": f"{json.dumps(good_record)}",
                  "topic_override": "inlake-test"},
            files={'file': ('filetitle', data)}
        )
        
        print(response.json())
        assert response.status_code == 404

def test_unsecured_BADrecord_json_bin():
    with TestClient(app) as client:
        schema = "test_schema_jb"
        s3_symbolic = "minio://astragale-testbucket/data.bin"
        inlkey = schema
        data = b'\x01'*2048
        good_record = {"firstname": "Bob", "lastname": "Du Buc Du Ferray", "job": "Pianiste", "__resource_path__": "nopath"}
        
        response = client.post(
            "/ingress/unsecured/record-json-and-binary",
            params={"inlk_key": f"{inlkey}", 
                  "content": f"{json.dumps(good_record)}",
                  "topic_override": "inlake-test"},
            files={'file': ('filetitle', data)}
        )
        
        print(response.json())
        assert response.status_code == 422