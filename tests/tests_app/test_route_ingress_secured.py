from fastapi.testclient import TestClient
from uuid import uuid4
from app.main import app
from datetime import datetime
from uuid import uuid4
import os
from pathlib import Path
import json
import pytest


from confluent_kafka.schema_registry.schema_registry_client import SchemaRegistryClient, Schema
import configparser

client = TestClient(app)

TEST_DIR =  Path(__file__).resolve().parents[1]
TEST_CONFIG_PATH = Path(TEST_DIR, '_test_config_inlake.cfg') #"C:/_Archi/Etudes/22ASTRA_/21ba_service-INlake/service-inlake/config_inlake.cfg"

# @pytest.fixture # Oui, ce serait plus propre avec une fixture
def sr_client() -> SchemaRegistryClient:
    main_config = configparser.ConfigParser()
    try:
        with open(TEST_CONFIG_PATH) as f:
            main_config.read_file(f)
    except IOError:
        raise FileNotFoundError("config.cfg file not found at CONFIG_PATH={}".format(TEST_CONFIG_PATH))

    session = SchemaRegistryClient(conf=dict(main_config['schema-registry']))
    return session

dummy_good_userschema_with_url = """
        {
            "type": "record",
            "name": "dummy_user",
            "namespace": "org.test.dummyuser",
            "fields": [
                {
                "name": "firstname",
                "type": "string"
                },
                {
                "name": "lastname",
                "type": "string"
                },
                {
                "name": "job",
                "type": "string"
                },
                {
                "name": "__resource_path__",
                "type": "string"
                }
            ]
        }
    """


#########
# TESTS
#########

# def test_read_ingress():
#     response = client.get("/ingress")
#     assert response.status_code == 200
#     assert response.json() == {"msg": "Hello Ingress World"}

# def test_unsecure_record_json():
#     with TestClient(app) as client: 
#         dtnow = f'{datetime.now():%Y-%m-%d--%H-%M-%S%z}'
#         key = f"unsecured-record-{dtnow}"
#         good_dummy_record = {"firstname": "Bob", "lastname": "Du Buc Du Ferray", "job": "Pianiste"}
        
#         response = client.post(
#                 "/ingress/record-json-unsecured",
#                 json={"kafka_key": f"{key}", "jsondata": f"{json.dumps(good_dummy_record)}"}
#             )
        
#         print(response.json())
#         assert response.status_code == 201
#         # assert False

@pytest.mark.skip(reason="temporary disabled secured tests")
def test_record_json_key_schemaonly():
    with TestClient(app) as client:
        sr = sr_client() 
        kafka_key = "dummy_unsecured"
        # new_sch = Schema(schema_str=dummy_good_userschema_with_url, schema_type='AVRO')
        # sr.register_schema(subject_name=kafka_key, schema=new_sch)

        good_dummy_record = {"firstname": "Bob", "lastname": "Du Buc Du Ferray", "job": "Pianiste", "__resource_path__": "www.jambon.musical"}
        
        response = client.post(
                "/ingress/record-json-unsecured",
                json={"kafka_key": f"{kafka_key}", "content": f"{json.dumps(good_dummy_record)}"}
            )
        
        print(response.json())

        # sr.delete_subject(subject_name=kafka_key)
        assert response.status_code == 201

@pytest.mark.skip(reason="temporary disabled secured tests")
def test_record_json_key_schema_and_destination():
    with TestClient(app) as client:
        sr = sr_client() 
        key_schema = "dummy_userschema_with_url"
        key_destination = "dbdummy"
        kafka_key = key_schema + '/' + key_destination
        new_sch = Schema(schema_str=dummy_good_userschema_with_url, schema_type='AVRO')
        sr.register_schema(subject_name=key_schema, schema=new_sch)

        good_dummy_record = {"firstname": "Bob", "lastname": "Du Buc Du Ferray", "job": "Pianiste", "__resource_path__": "www.jambon.musical"}
        
        response = client.post(
                "/ingress/record-json",
                json={"kafka_key": f"{kafka_key}", "content": f"{json.dumps(good_dummy_record)}"}
            )
        
        print(response.json())

        sr.delete_subject(subject_name=key_schema)
        assert response.status_code == 201


# def test_record_upload():
#     with TestClient(app) as client:
#         key = "dummy_userschema"
#         good_dummy_record = {"firstname": "Mob", "lastname": "Du Muc Du Berray", "job": "Mianiste"}
#         data = b'\x01'*2048

#         response = client.post(
#                 "/ingress/upload",
#                 params={"kafka_key": f"{key}", "content": f"{json.dumps(good_dummy_record)}"},
#                 files={'file': ('nomdufichier', data)}
#             )
        
#         print(response.json())
#         assert response.status_code == 200

@pytest.mark.skip(reason="temporary disabled secured tests")
def test_record_jsonbinary_schemaonly():
    with TestClient(app) as client:
        sr = sr_client() 
        kafka_key = "dummy_userschema_with_url"
        new_sch = Schema(schema_str=dummy_good_userschema_with_url, schema_type='AVRO')
        sr.register_schema(subject_name=kafka_key, schema=new_sch)

        test_topic = 'inlake-gateway-test'
        good_dummy_record = {"firstname": "Mob", "lastname": "Du Muc Du Berray", "job": "Mianiste", "__resource_path__": ""}
        bname = 'astragale-testbucket'
        data = b'\x01'*2048

        response = client.post(
                "/ingress/record-json-and-binary",
                params={"kafka_key": f"{kafka_key}", 
                        "content": f"{json.dumps(good_dummy_record)}", 
                        "filesys_id": "minio",
                        "filesys_dirpath": bname, 
                        "topic_override": test_topic},
                files={'file': ('nomdufichier', data)}
            )
        
        print(response.json())
        sr.delete_subject(subject_name=kafka_key)

        assert response.status_code == 201

@pytest.mark.skip(reason="temporary disabled secured tests")
def test_record_jsonbinary_schema_and_destination():
    with TestClient(app) as client:
        sr = sr_client() 
        key_schema = "dummy_userschema_with_url"
        key_destination = "dbdummy"
        kafka_key = key_schema + '/' + key_destination
        
        new_sch = Schema(schema_str=dummy_good_userschema_with_url, schema_type='AVRO')
        sr.register_schema(subject_name=key_schema, schema=new_sch)

        test_topic = 'inlake-gateway-test'
        good_dummy_record = {"firstname": "Mob", "lastname": "Du Muc Du Berray", "job": "Mianiste", "__resource_path__": ""}
        bname = 'astragale-testbucket'
        data = b'\x01'*2048

        response = client.post(
                "/ingress/record-json-and-binary",
                params={"kafka_key": f"{kafka_key}", 
                        "content": f"{json.dumps(good_dummy_record)}", 
                        "filesys_id": "minio",
                        "filesys_dirpath": bname, 
                        "topic_override": test_topic},
                files={'file': ('nomdufichier', data)}
            )
        
        print(response.json())
        sr.delete_subject(subject_name=key_schema)

        assert response.status_code == 201