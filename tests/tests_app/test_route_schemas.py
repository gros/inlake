# from fastapi.testclient import TestClient
# from uuid import uuid4
# from app.main import app
# from datetime import datetime
# import configparser
# from inlake.schemaregistry_basicapi import InlkSchemaRClient
# import pytest
# from uuid import uuid4
# import os
# from pathlib import Path
# import json

# from confluent_kafka.schema_registry.schema_registry_client import Schema, SchemaReference, SchemaRegistryClient, RegisteredSchema

# client = TestClient(app)

# TEST_DIR =  Path(__file__).resolve().parents[1]
# TEST_CONFIG_PATH = Path(TEST_DIR, '_test_config_inlake.cfg') #"C:/_Archi/Etudes/22ASTRA_/21ba_service-INlake/service-inlake/config_inlake.cfg"

# dummy_good_avro_raw = """
#         {
#             "type": "record",
#             "name": "dummy_user",
#             "namespace": "org.test.dummyuser",
#             "fields": [
#                 {
#                 "name": "firstname",
#                 "type": "string"
#                 },
#                 {
#                 "name": "lastname",
#                 "type": "string"
#                 },
#                 {
#                 "name": "job",
#                 "type": "string"
#                 }
#             ]
#         }
#     """

# # @pytest.fixture
# def sr_client() -> SchemaRegistryClient:
#     main_config = configparser.ConfigParser()
#     try:
#         with open(TEST_CONFIG_PATH) as f:
#             main_config.read_file(f)
#     except IOError:
#         raise FileNotFoundError("config.cfg file not found at CONFIG_PATH={}".format(TEST_CONFIG_PATH))

#     session = SchemaRegistryClient(conf=dict(main_config['schema-registry']))
#     return session

# def test_read_schemas():
#     response = client.get("/schemas")
#     assert response.status_code == 200
#     assert response.json() == {"msg": "Hello Schemas World"}

# #############################
# # Get / List / CheckExists
# #############################
# def test_list_schemas():
#     with TestClient(app) as client: 
#         response = client.get("/schemas/list-schemas")
#         assert response.status_code == 200
#         assert response.json()['from'] == 'http://localhost:8081'

# def test_get_schema():
#     sr = sr_client()
#     dtnow = f'{datetime.now():%Y-%m-%d--%H-%M-%S%z}'
#     to_get_schema_name = "{}___test-GET".format(dtnow)
#     response = client.post(
#             "/schemas/create-avro-schema",
#             json={"subject_name": f"{to_get_schema_name}", "raw_str": f"{dummy_good_avro_raw}"}
#         )
#     assert response.status_code == 201
#     assert response.json()['msg'] == f"Created schema {to_get_schema_name} on Schema Registry"

#     response_get = client.get(f"/schemas/get/{to_get_schema_name}")
#     assert response_get.status_code == 200
#     assert response_get.json()['subject'] == f"{to_get_schema_name}"

#     sr.delete_subject(subject_name=to_get_schema_name)

#     response_falseget = client.get(f"/schemas/get/lolno-{dtnow}")
#     assert response_falseget.status_code == 404

# def test_check_schema_existence():
#     sr = sr_client()
#     ### Existing
#     dtnow = f'{datetime.now():%Y-%m-%d--%H-%M-%S%z}'
#     my_test_subject_name = "{}___test-EXISTING".format(dtnow)
#     new_sch = Schema(schema_str=dummy_good_avro_raw, schema_type='AVRO')
#     sr.register_schema(subject_name=my_test_subject_name, schema=new_sch)

#     response = client.post(
#         "/schemas/check-schema-existence",
#         json={"subject_name": f"{my_test_subject_name}", "raw_str": "strstr"}
#     )
#     assert response.status_code == 200
#     assert response.json()['msg'] == f'Schema {my_test_subject_name} already exists on Schema Registry'

#     # sr_client.delete_subject(subject_name=my_test_subject_name)

#     ### Non-Existing
#     my_bad_test_subject_name = "lolno+a515e5a6-b9c7-430d-8e85-32ddc4e5c4e1"
#     response = client.post(
#         "/schemas/check-schema-existence",
#         json={"subject_name": f"{my_bad_test_subject_name}", "raw_str": "strstr"}
#     )
#     assert response.status_code == 204
#     # assert response.json()['msg'] == f'Schema {my_bad_test_subject_name} does not exist on Schema Registry'

#     sr.delete_subject(subject_name=my_test_subject_name)


# ##############################################
# # Validation / Creation / Deletion
# ##############################################
# # @pytest.fixture
# def test_new_schema():
#     sr = sr_client() 
#     ### Good
#     dtnow = f'{datetime.now():%Y-%m-%d--%H-%M-%S%z}'
#     new_schema_name = "{}___test-NEW".format(dtnow)
#     response = client.post(
#             "/schemas/create-avro-schema",
#             json={"subject_name": f"{new_schema_name}", "raw_str": f"{dummy_good_avro_raw}"}
#         )
#     assert response.status_code == 201
#     assert response.json()['msg'] == f"Created schema {new_schema_name} on Schema Registry"

#     ### Bad avro format
#     # dtnow = f'{datetime.now():%Y-%m-%d--%H-%M-%S%z}'
#     # new_bad_schema_name = "badtest__{}".format(dtnow)
#     # bad__avro_schema = """{
#     #             "type": "record",
#     #             "name": "dummy_user",
#     #             "namespace": "org.test.dummyuser"}
#     #             """
#     # response = client.post(
#     #         "/schemas/create-avro-schema",
#     #         json={"subject_name": f"{new_bad_schema_name}", "raw_str": f"{bad__avro_schema}"}
#     #     )
#     # assert response.status_code == 422

#     sr.delete_subject(subject_name=new_schema_name)

# ##############################################
# # DATA Validation
# ##############################################
# def test_validate_data_against():
#     sr = sr_client() 
#     ### Good
#     dtnow = f'{datetime.now():%Y-%m-%d--%H-%M-%S%z}'
#     valid_schema_name = "{}___test-VALIDATION".format(dtnow)
#     response = client.post(
#             "/schemas/create-avro-schema",
#             json={"subject_name": f"{valid_schema_name}", "raw_str": f"{dummy_good_avro_raw}"}
#         )
#     assert response.status_code == 201
#     assert response.json()['msg'] == f"Created schema {valid_schema_name} on Schema Registry"

#     good_dummy_record = {"firstname": "Bob", "lastname": "Du Buc Du Ferray", "job": "Pianiste"}
#     # sch = json.load(good_dummy_record)
#     # ftext = json.dumps(good_dummy_record)
#     r2 = client.post(
#             f"/schemas/validate-data-compliance/{valid_schema_name}",
#             json={"raw_str": f"{good_dummy_record}"}
#         )
#     print(r2.text)
#     assert r2.status_code == 200

#     sr.delete_subject(subject_name=valid_schema_name)
