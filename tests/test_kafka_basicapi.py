from inlake.kafka_basicapi import basicKafkaAPI
from pathlib import Path
import configparser
import pytest
from uuid import uuid4
import os


WORKDIR                 = Path(__file__).parent.resolve()
CONFIG_PATH             = Path(WORKDIR, './_test_config_inlake.cfg')


@pytest.fixture
def basic_kafka_api():
    config = configparser.ConfigParser()
    try:
        with open(CONFIG_PATH) as f:
            config.read_file(f)
    except IOError:
        raise FileNotFoundError("config.cfg file not found at CONFIG_PATH={}".format(CONFIG_PATH))
    yield basicKafkaAPI(configs=dict(config['kafka']))


@pytest.mark.usefixtures("basic_kafka_api")
class TestbasicKafkaAPI():
    __test__ = False
     
    def test_healtcheck(self, basic_kafka_api):
        assert basic_kafka_api.healthcheck() is True

    def test_topic_creation(self, basic_kafka_api):
        test_topic_name = "abasicapi_test_topic+"+ str(uuid4())
        basic_kafka_api.create_topic(topic_name=test_topic_name)
        assert (test_topic_name in basic_kafka_api.adminclient.list_topics(test_topic_name).topics)
        basic_kafka_api.adminclient.delete_topics([test_topic_name])

    def test_produce_message(self, basic_kafka_api):
        test_topic_name = "abasicapi_messageprod_topic"# + str(uuid4())
        basic_kafka_api.create_topic(topic_name=test_topic_name)

        basic_kafka_api.produce_message(topic=test_topic_name, key="clefmsg", value="valuemsg")

        # basic_kafka_api.adminclient.delete_topics([test_topic_name])
