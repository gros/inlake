#######################
# Logger setup from cfg
#######################
import logging
from logging.config import dictConfig
log_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": "%(levelprefix)s %(asctime)s %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",

        },
    },
    "handlers": {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
        },
    },
    "loggers": {
        "inl-logger": {"handlers": ["default"], "level": "DEBUG"},
        "sch-logger": {"handlers": ["default"], "level": "DEBUG"},
        "ing-logger": {"handlers": ["default"], "level": "DEBUG"},
    },
    
}
dictConfig(log_config)

inllogger = logging.getLogger('inl-logger')
s_log = logging.getLogger('inl-sch-logger')
i_log = logging.getLogger('inl-ing-logger')