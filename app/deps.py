from app.sessions import client_schema, client_kafka, global_config
from inlake.schemaregistry_basicapi import InlkSchemaRClient
from inlake.kafka_aioapi import AIOKafkaApi
import configparser

def get_global_config() -> configparser.ConfigParser:
    return global_config

def get_client_schema() -> InlkSchemaRClient:
    return client_schema

def get_client_kafka() -> AIOKafkaApi:
    return client_kafka