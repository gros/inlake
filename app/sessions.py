from inlake.schemaregistry_basicapi import InlkSchemaRClient
from inlake.kafka_aioapi import AIOKafkaApi
import configparser

global_config = configparser.ConfigParser()
client_schema = InlkSchemaRClient()
client_kafka = AIOKafkaApi()
my_global = None