import os
from datetime import datetime
from typing import (Union, Tuple, Optional)
import configparser
import json
import uuid
from urllib.parse import urlparse
import ast

from fastapi import APIRouter, HTTPException, Depends, Response, status, File, UploadFile
from pydantic import BaseModel

from app.deps import get_client_schema, get_client_kafka, get_global_config
from inlake.schemaregistry_basicapi import InlkSchemaRClient
from inlake.kafka_aioapi import AIOKafkaApi
from aiobotocore.session import get_session

import logging
from app.loggers import i_log

from dotenv import load_dotenv
load_dotenv()


router = APIRouter(
    prefix="/ingress",
    tags=["ingress"],
    # dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Operation on ingress not found"}},
)

class Record(BaseModel):
    inlk_key:                str             
    content:                 str
    filesys_dirpath:         Optional[str]      # for minio, filesys_dirpath is bucket path
    topic_override:          Optional[str]


def inlk_to_kafka_key(my_inlk_key: str) -> Tuple[str, str]:
    duuid = str(uuid.uuid4())[:8]
    kkey = f"{duuid}://{my_inlk_key}"
    return kkey, duuid

def parse_filesys_dirpath(s: str) -> Tuple[str, str]:
    o = urlparse(s, allow_fragments=False)
    return o.scheme, o.netloc, o.path


#                                __
#   ___ ___ ______ _________ ___/ /
#  (_-</ -_) __/ // / __/ -_) _  / 
# /___/\__/\__/\_,_/_/  \__/\_,_/  

@router.post("/record-json")
async def produce_record_json(  record: Record, response: Response,
                                client_kafka: AIOKafkaApi = Depends(get_client_kafka),
                                client_schema: InlkSchemaRClient = Depends(get_client_schema),
                                global_config: configparser.ConfigParser = Depends(get_global_config)):
    """Send a record to inlake.records.topic with the schema name as key
    Record's content is validated against the provided schema.
    The key can be formated as follow : 
        + "SCHEMA": specifying only the schema to be used
        + "SCHEMA/DESTINATION": specifying the schema and the database/part of database to be sent to. Convenient for debugging of Named Graphs in Knowledge Bases

    Args:
        record (Record): pydantic model for sent content, format query with params={"kafka_key": str, "content": str}
        response (Response): handler for fastapi's starlette http response
        client_kafka (AIOKafkaApi, dependance): asynchronous client for Kafka Producer. Defaults to Depends(get_client_kafka).
        client_schema (InlkSchemaRClient, dependance): exposes synchronous client for confluent_kafka Schema Registry. Defaults to Depends(get_client_schema).
        global_config (configParser, dependance): configuration. Defaults to Depends(get_global_config).

    Raises:
        HTTPException (HTTP_404_NOT_FOUND): when schema named "kafka_key" is not found on Schema Registry
        HTTPException (HTTP_422_UNPROCESSABLE_ENTITY): when record's content is not compliant with schema

    Returns:
        httpResponse: HTTP_200_OK for successfully sent message with record
    """
    secure_topic = global_config['kafka-topics']['inlake.records.topic']

    key_schema = None
    key_destination = None

    if '/' in record.inlk_key:
        # case KAFKA_KEY formatted as "SCHEMA/DESTINATION"
        key_schema = (record.inlk_key).split('/')[0]
        key_destination = (record.inlk_key).split('/')[1]
        i_log.info(f"INGRESS Trying to upload {record.content[:20]} with key {key_schema}/{key_destination} to topic inlake-gateway")
    else:
        # case KAFKA_KEY formatted as "SCHEMA"
        key_schema = record.inlk_key
        i_log.info(f"INGRESS Trying to upload {record.content[:20]} with key {key_schema} (no destination) to topic inlake-gateway")
    
    
    # Getting Schema
    try:
        getted_sr_schema = client_schema.sr.get_latest_version(key_schema)
    except BaseException as e:
        raise HTTPException(status_code=404, detail=f"Schema {key_schema} not found, {e}")
    
    # Validating and producing message
    i_log.info(f"VALIDATING DATA AGAINST {key_schema}")
    if client_schema.validate_data_against_schema(schema=getted_sr_schema.schema, input_data=json.loads(record.content.replace("'", '"'))) is True:
        await client_kafka.produce_message_str(topic=secure_topic, key=record.inlk_key, value=record.content)
        
        response.status_code=status.HTTP_201_CREATED
        return {"msg": f"INGRESS record sent with data {record.content[20:]} with key {record.inlk_key} to topic inlake-gateway"}
    
    else:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=f"Inputted data is not compliant with schema {key_schema}")


@router.post("/record-json-and-binary")
async def produce_record_json_n_binary( response: Response,
                                        record: Record = Depends(), 
                                        file: UploadFile = File(...),
                                        client_kafka: AIOKafkaApi = Depends(get_client_kafka),
                                        client_schema: InlkSchemaRClient = Depends(get_client_schema),
                                        global_config: configparser.ConfigParser = Depends(get_global_config)):
    """Send a record to inlake.records.topic with a) the schema name as key ; b) data file uploaded to s3 bucket
    Record's content is validated against the provided schema.
    Record's content features the uri adress for the uploaded data file

    Record's content is validated against the provided schema.
    The key can be formated as follow : 
        + "SCHEMA": specifying only the schema to be used
        + "SCHEMA/DESTINATION": specifying the schema and the database/part of database to be sent to. Convenient for the use of Named Graphs in Knowledge Bases

    TODO: The configuration of different filesystems put behind inlake is hardcoded for now, put it in .cfg if needed

    Args:
        response (Response): handler for fastapi's starlette http response
        record (Record): pydantic model for sent content, format query with params={"kafka_key": str, "content": str}
        file (UploadFile): binary file for s3 bucket upload. Defaults to File(...).
        client_kafka (AIOKafkaApi, dependance): asynchronous client for Kafka Producer. Defaults to Depends(get_client_kafka).
        client_schema (InlkSchemaRClient, dependance): exposes synchronous client for confluent_kafka Schema Registry. Defaults to Depends(get_client_schema).
        global_config (configParser, dependance): configuration. Defaults to Depends(get_global_config).

    Raises:
        HTTPException (HTTP_404_NOT_FOUND): when schema named "kafka_key" is not found on Schema Registry
        HTTPException (HTTP_422_UNPROCESSABLE_ENTITY): when schema does not feature a uri_adress to reference the s3 file upload
        HTTPException (HTTP_422_UNPROCESSABLE_ENTITY): when record's content is not compliant with schema
        HTTPException (HTTP_404_NOT_FOUND): when the bucket is not found on the s3 server

    Returns:
        httpResponse: HTTP_200_OK for successfully sent message with record
    """
    
    # GLOBALS
    # Kafka-Topic
    if record.topic_override is None:
        secure_topic = global_config['kafka-topics']['inlake.records.topic']
    else:
        secure_topic = record.topic_override  
    
    # SCHEMA
    key_schema = None
    key_destination = None

    if '/' in record.inlk_key:
        # case KAFKA_KEY formatted as "SCHEMA/DESTINATION"
        key_schema = (record.inlk_key).split('/')[0]
        key_destination = (record.inlk_key).split('/')[1]
        i_log.info(f"INGRESS Trying to upload {record.content[:20]} with key {key_schema}/{key_destination} to topic inlake-gateway")
    else:
        # case KAFKA_KEY formatted as "SCHEMA"
        key_schema = record.inlk_key
        i_log.info(f"INGRESS Trying to upload {record.content[:20]} with key {key_schema} (no destination) to topic inlake-gateway")

    # Schema-get
    try:
        getted_sr_schema = client_schema.sr.get_latest_version(key_schema)
    except BaseException as e:
        raise HTTPException(status_code=404, detail=f"Schema {key_schema} not found, {e}")
    
    
    # Schema-Validate
    # if there is a source in the schema TODO: do this in validate_data_against_schema function, with different behavior binary/non binary for Avro and Protobuf schemas
    # Too arbitrary, must be more organic
    if getted_sr_schema.schema.schema_type == 'AVRO':
        sc = json.loads(getted_sr_schema.schema.schema_str)
        i_log.info(f"SCHEMA Current Schema fields are: {[items['name'] for items in sc['fields']]}")
        if ('__resource_path__' or 'db_destination') not in [items['name'] for items in sc['fields']]:
            raise HTTPException(status_code=422, detail=f"The provided AVRO schema must reference a '__resource_path__' or 'db_destination' entry for the uploaded binary ; schema fields={sc['fields']}")


    if record.filesys_id == 'minio':
        # TODO: Build a function that does the entire job, avoid the big if...else (match...case would be better)
        if record.filesys_dirpath is not None:
            if '/' in record.filesys_dirpath:
                s3_bname = record.filesys_dirpath.split('/')[0]
                s3_filepath = record.filesys_dirpath.split('/')[1] # Bricolage honteux, mettre un regex avec lookforward dès le premier /
            else:
                s3_bname = record.filesys_dirpath
                s3_filepath = file.filename
                # s3_filepath = '{}/{}'.format(record.filesys_dirpath, file.filename)
        else:
            i_log.warning(f"No filesys_dirpath in record, defaulting to 'astragale-testbucket'")

            # s3_adress_key = '{}/{}'.format("astragale-testbucket", file.filename)
        s3_symbolic_adress = f"{global_config['s3-internal']['endpoint_url']}/{s3_bname}/{s3_filepath}"
        i_log.info(f"INGRESS Uploading record content={record.content[20:]}, s3_symbolic_adress=: {s3_symbolic_adress}, schema={record.inlk_key} to topic: {secure_topic}")
     
    else:
        raise HTTPException(status_code=404, detail=f"Filesystem {record.filesys_id} not found")   


    # RECORD
    content_asdict = json.loads(record.content.replace("'", '"')) # TODO: virer cet horrible replace
    
    
    # Record-urlchecker
    i_log.info(f"CONTENT_ASDICT KEYS {content_asdict.keys()}")
    i_log.info(f"RECORD Data url validation with adress {s3_symbolic_adress}")
    if content_asdict['__resource_path__'] != s3_symbolic_adress:
        i_log.warning(f"RECORD Data url changed from '{content_asdict['__resource_path__']}' to '{s3_symbolic_adress}'")
        content_asdict['__resource_path__'] = s3_symbolic_adress

    # Record-Compliancy
    i_log.info(f"RECORD Validate content compliance with schema: {key_schema}")
    if client_schema.validate_data_against_schema(schema=getted_sr_schema.schema, input_data=content_asdict) is False:                
        raise HTTPException(status_code=422, detail=f"VALIDATION ERROR, the provided record content does not fit the schema ; schema fields={sc['fields']}")
        
    # S3
    # Connect-session
    # s3_adress = None
    s3_object_acl = None
    session = get_session()
    async with session.create_client('s3',
                                        aws_access_key_id=global_config['s3-internal']['aws_access_key_id'],
                                        aws_secret_access_key=global_config['s3-internal']['aws_secret_access_key'],
                                        endpoint_url=global_config['s3-internal']['endpoint_url']
                                        ) as client:

        # 1. Bucket
        # Asserting the existence of the bucket in the S3 server
        ls_buckets_meta = await client.list_buckets()
        ls_buckets = [bucket['Name'] for bucket in ls_buckets_meta['Buckets']]
        if (s3_bname not in ls_buckets):
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"A valid s3 bucket must be specified for binary upload, provided={record.filesys_dirpath} ; available buckets={ls_buckets}")

        # 2. Key is s3_adress_key
        # TODO: assert if there is already a file at this key

        # 3. Content
        data = await file.read()

        # PUT OBJECT
        r2 = await client.put_object(   Bucket=s3_bname,
                                        Key=s3_filepath,
                                        Body=data)
        i_log.info(f"S3 uploaded data to {s3_symbolic_adress}")

        # getting s3 object properties of file we just uploaded
        s3_object_acl = await client.get_object_acl(Bucket=s3_bname, Key=s3_filepath)
        i_log.info(f"S3 uploaded object acls {s3_object_acl}")

    await file.close()

    await client_kafka.produce_message_str(topic=secure_topic, key=record.inlk_key, value=json.dumps(content_asdict))
    response.status_code=status.HTTP_201_CREATED
    return {"msg": f"INGRESS data sent to adress={s3_symbolic_adress}, with record and schema={record.inlk_key} to topic={secure_topic}"}


#                                         __
#  __ _____  ___ ___ ______ _________ ___/ /
# / // / _ \(_-</ -_) __/ // / __/ -_) _  / 
# \_,_/_//_/___/\__/\__/\_,_/_/  \__/\_,_/  
                                          
@router.post("/unsecured/record-json")
async def produce_record_json_unsecured(    record: Record, response: Response,
                                            client_kafka: AIOKafkaApi = Depends(get_client_kafka),
                                        ):
    """Send a record to inlake.records.topic with the schema name as key
    Record's content is NOT validated against the provided schema.
    The key can be formated as follow : 
        + "SCHEMA": specifying only the schema to be used
        + "SCHEMA/DESTINATION": specifying the schema and the database/part of database to be sent to. Convenient for debugging of Named Graphs in Knowledge Bases

    Args:
        record (Record): pydantic model for sent content, format query with params={"kafka_key": str, "content": str}
        response (Response): handler for fastapi's starlette http response
        client_kafka (AIOKafkaApi, dependance): asynchronous client for Kafka Producer. Defaults to Depends(get_client_kafka).
        client_schema (InlkSchemaRClient, dependance): exposes synchronous client for confluent_kafka Schema Registry. Defaults to Depends(get_client_schema).
        global_config (configParser, dependance): configuration. Defaults to Depends(get_global_config).

    Raises:
        HTTPException (HTTP_404_NOT_FOUND): when schema named "kafka_key" is not found on Schema Registry
        HTTPException (HTTP_422_UNPROCESSABLE_ENTITY): when record's content is not compliant with schema

    Returns:
        httpResponse: HTTP_200_OK for successfully sent message with record
    """
    k_topic = record.topic_override or os.environ['INLAKE_TOPIC_INGRESS_UNSECURED']

    kkey, duuid = inlk_to_kafka_key(record.inlk_key)
    i_log.info(f"Producing json-record with key {kkey} to Kafka topic {k_topic}")    
    
    await client_kafka.produce_message_str(topic=k_topic, key=kkey, value=record.content)
    
    response.status_code=status.HTTP_201_CREATED
    return {"msg": f"INGRESS json-record with key={kkey} ; to Kafka topic={k_topic}", "duuid": {duuid}}


@router.post("/unsecured/record-json-and-binary")
async def produce_record_json_n_binary_unsecured( response: Response,
                                        record: Record = Depends(), 
                                        file: UploadFile = File(...),
                                        client_kafka: AIOKafkaApi = Depends(get_client_kafka)
                                        ):
    """Send a record to inlake.records.topic with a) the schema name as key ; b) data file uploaded to s3 bucket
    Record's content is validated against the provided schema.
    Record's content features the uri adress for the uploaded data file

    Record's content is validated against the provided schema.
    The key can be formated as follow : 
        + "SCHEMA": specifying only the schema to be used
        + "SCHEMA/DESTINATION": specifying the schema and the database/part of database to be sent to. Convenient for the use of Named Graphs in Knowledge Bases

    TODO: The configuration of different filesystems put behind inlake is hardcoded for now, put it in .cfg if needed

    Args:
        response (Response): handler for fastapi's starlette http response
        record (Record): pydantic model for sent content, format query with params={"kafka_key": str, "content": str}
        file (UploadFile): binary file for s3 bucket upload. Defaults to File(...).
        client_kafka (AIOKafkaApi, dependance): asynchronous client for Kafka Producer. Defaults to Depends(get_client_kafka).
        client_schema (InlkSchemaRClient, dependance): exposes synchronous client for confluent_kafka Schema Registry. Defaults to Depends(get_client_schema).
        global_config (configParser, dependance): configuration. Defaults to Depends(get_global_config).

    Raises:
        HTTPException (HTTP_404_NOT_FOUND): when schema named "kafka_key" is not found on Schema Registry
        HTTPException (HTTP_422_UNPROCESSABLE_ENTITY): when schema does not feature a uri_adress to reference the s3 file upload
        HTTPException (HTTP_422_UNPROCESSABLE_ENTITY): when record's content is not compliant with schema
        HTTPException (HTTP_404_NOT_FOUND): when the bucket is not found on the s3 server

    Returns:
        httpResponse: HTTP_200_OK for successfully sent message with record
    """
    
    # topic
    k_topic = record.topic_override or os.environ['INLAKE_TOPIC_INGRESS_UNSECURED']

    # keys&content
    kkey, duuid = inlk_to_kafka_key(record.inlk_key)

    try:
        content_asdict = ast.literal_eval(record.content)
        filesys_id, s3bucket_name, s3path = parse_filesys_dirpath(content_asdict['__resource_path__'])
    except Exception as e:
        i_log.error(e)
        raise HTTPException(status_code=422, 
                            detail=f"File destination shall be written in a '__resource_path__' field in json-params ; {record.content}")

    i_log.info(f"(json) Producing metadatas json-record with key '{kkey}' to Kafka topic '{k_topic}'")

    # FILESYS
    if filesys_id == 'minio':
        i_log.info(f"(file) s3-minio, Pushing to {filesys_id}://{s3bucket_name}{s3path} with minio={os.environ['MINIO_HOST']}") # s3_symbolic_adress
        
        s3_object_acl = None
        session = get_session()
        async with session.create_client(   's3',
                                            aws_access_key_id=os.environ['MINIO_ROOT_USER'],
                                            aws_secret_access_key=os.environ['MINIO_ROOT_PASSWORD'],
                                            endpoint_url=os.environ['MINIO_HOST']
                                            ) as client:

            # 1. Bucket
            # Asserting the existence of the bucket in the S3 server
            ls_buckets_meta = await client.list_buckets()
            ls_buckets = [bucket['Name'] for bucket in ls_buckets_meta['Buckets']]
            if (s3bucket_name not in ls_buckets):
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"A valid s3 bucket must be specified for binary upload, provided={record.filesys_dirpath} ; available buckets={ls_buckets}")
            # 2. Key is s3_adress_key
            # TODO: assert if there is already a file at this key

            # 3. Content
            data = await file.read()

            # PUT OBJECT
            r2 = await client.put_object(   Bucket=s3bucket_name,
                                            Key=s3path,
                                            Body=data)
            i_log.info(f"(file) s3-minio, success")

            # getting s3 object properties of file we just uploaded
            s3_object_acl = await client.get_object_acl(Bucket=s3bucket_name, Key=s3path)
            i_log.info(f"(file) s3-minio, acls={s3_object_acl}")

            await file.close()
    else:
        raise HTTPException(status_code=422, 
                            detail=f"Filesystem {filesys_id} must exist and File destination shall be written in a '__resource_path__' field in json-params ; {record.content}")


    # # KAFKA
    await client_kafka.produce_message_str(topic=k_topic, key=kkey, value=record.content)
    
    response.status_code=status.HTTP_201_CREATED
    return {"msg": f"INGRESS json-record with key={kkey} ; to Kafka topic={k_topic}", "duuid": {duuid}}