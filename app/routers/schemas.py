from fastapi import APIRouter, HTTPException, Depends, Response, status
from confluent_kafka.schema_registry.schema_registry_client import Schema
from confluent_kafka.schema_registry.error import SchemaRegistryError
from inlake.schemaregistry_basicapi import InlkSchemaRClient
from pydantic import BaseModel
import confluent_kafka
import json

import logging
from app.loggers import s_log

from app.deps import get_client_schema


router = APIRouter(
    prefix="/schemas",
    tags=["schemas"],
    # dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Operation on schemas not found"}},
)


class ModelSchema(BaseModel):
    """
        Multiple inheritance BaseModel, Schema results in a conflict
    """
    subject_name: str
    raw_str: str

class ModelSchemaName(BaseModel):
    subject_name: str

class ModelSchemaSchema(BaseModel):
    raw_str: str


@router.get("/")
async def helloworld_schemas():
    return {"msg": "Hello Schemas World"}

##################################
# SCHEMA Get / List / CheckExists
##################################
@router.get("/get/{schema_name}")
async def get_schema(schema_name: str, 
                     response: Response, 
                     client_schema: InlkSchemaRClient = Depends(get_client_schema)):
    """Get the string representation of the schema "schema_name" in Schema Registry

    Args:
        schema_name (str): the name of the schema/subject to get from Schema Registry
        response (Response): handler for fastapi's starlette http response
        client_schema (InlkSchemaRClient, dependance): exposes synchronous client for confluent_kafka Schema Registry. Defaults to Depends(get_client_schema).

    Raises:
        HTTPException (HTTP_404_NOT_FOUND): when schema named "schema_name" is not found on Schema Registry

    Returns:
        json: the schema name as "subject" and its str representation as "schema_str"
    """
    # if schema_name in [subject for subject in client_schema.sr.get_subjects()]:
    try:
        getted_sr_schema = client_schema.sr.get_latest_version(schema_name)
        response.status_code=status.HTTP_200_OK
        return {"subject": f"{schema_name}", "schema_str": f"{getted_sr_schema.schema.schema_str}"}
    except BaseException as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Schema {schema_name} not found, {e}")


@router.get("/list-schemas")
async def get_list_of_schemas(client_schema: InlkSchemaRClient = Depends(get_client_schema)):
    """Lists the schemas available on the Schema Registry

    Args:
        client_schema (InlkSchemaRClient, dependance): exposes synchronous client for confluent_kafka Schema Registry. Defaults to Depends(get_client_schema).

    Returns:
        json: "from", Schema Registry provenance ; "msg" : the list of the schemas
    """
    s_log.info(f"SR Listing Schemas on url {client_schema.url}")
    list_of_schemas = [subject for subject in client_schema.sr.get_subjects()]
    return {"from": f"{client_schema.url}", "msg": f"{list_of_schemas}"}

@router.post("/check-schema-existence")
async def check_existence(modelschemaname: ModelSchemaName, response: Response, client_schema: InlkSchemaRClient = Depends(get_client_schema)):
    s_log.info(f"SR Checking existence of Schema named {modelschemaname.subject_name}")
    if client_schema.check_existence(subject_name=modelschemaname.subject_name):
        response.status_code=status.HTTP_200_OK
        return {"msg": f"Schema {modelschemaname.subject_name} already exists on Schema Registry"}
    else:
        response.status_code=status.HTTP_204_NO_CONTENT
        return {"msg": f"Schema {modelschemaname.subject_name} does not exist on Schema Registry"}

##############################################
# SCHEMA Validation / Creation / Deletion
##############################################
@router.post("/create-avro-schema")
async def new_schema(modelschema: ModelSchema, response: Response, client_schema: InlkSchemaRClient = Depends(get_client_schema)):
    s_log.info(f"SR Trying to register Schema named {modelschema.subject_name}")
    if client_schema.check_existence(subject_name=modelschema.subject_name) is True:
        raise HTTPException(status_code=400, detail=f"Subject: {modelschema.subject_name}, is already a registered schema. Existing schema will be used. (Please, Update and Manage registered schemas through UI") 
    
    try:
        new_sch= Schema(schema_str=modelschema.raw_str, schema_type='AVRO')
    except SchemaRegistryError as e:
        raise HTTPException(status_code=400, detail=f"Bad formatting, validation raised {e}")
    
    client_schema.sr.register_schema(subject_name=modelschema.subject_name, schema=new_sch)
    
    s_log.info(f"SR Registered Schema named {modelschema.subject_name}")
    response.status_code=status.HTTP_201_CREATED
    return {"msg": f"Created schema {modelschema.subject_name} on Schema Registry"}

@router.post("/validate-avro-schema")
async def validate_schema(modelschema: ModelSchema, response: Response, client_schema: InlkSchemaRClient = Depends(get_client_schema)):
    try:
        Schema(schema_str=modelschema.raw_str, schema_type='AVRO')
    except SchemaRegistryError as e:
        raise HTTPException(status_code=400, detail=f"Bad formatting, validation raised {e}")
    
    response.status_code=status.HTTP_200_OK
    return {"msg": f"Schema {modelschema.subject_name} is a valid Avro schema"}
    
@router.post("/delete-avro-schema")
async def delete_schema(modelschemaname: ModelSchemaName, response: Response, client_schema: InlkSchemaRClient = Depends(get_client_schema)):
    client_schema.sr.delete_subject(subject_name=modelschemaname.subject_name)
    response.status_code=status.HTTP_200_OK
    return {"msg": f"Deleted schema {modelschemaname.subject_name} on Schema Registry"}


##############################################
# DATA Validation
##############################################
@router.post("/validate-data-compliance/{schema_name}")
async def validate_data_against(schema_name: str, mss: ModelSchemaSchema, response: Response, client_schema: InlkSchemaRClient = Depends(get_client_schema)):
    # if schema_name in [subject for subject in client_schema.sr.get_subjects()]:
    try:
        getted_sr_schema = client_schema.sr.get_latest_version(schema_name)
    except BaseException as e:
        raise HTTPException(status_code=404, detail=f"Schema {schema_name} not found, {e}")

    # response.status_code=status.HTTP_200_OK
    # return {"msg": f"Data is compliant with schema {schema_name}"}

    s_log.info(f"VALIDATING DATA AGAINST {schema_name}")

    if client_schema.validate_data_against_schema(schema=getted_sr_schema.schema, input_data=json.loads(mss.raw_str.replace("'", '"'))) is True:
        response.status_code=status.HTTP_200_OK
        return {"msg": f"Data is compliant with schema {schema_name}"}