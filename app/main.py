import os
from pathlib import Path
import configparser

from fastapi import FastAPI
from app.sessions import client_schema, client_kafka, global_config
from app.loggers import inllogger
from app.routers import schemas, ingress

from dotenv import load_dotenv
load_dotenv()


# TEST_DIR =  Path(__file__).resolve().parents[1]
# DEFAULT_CONFIG_PATH = Path(TEST_DIR, 'config_inlake.cfg')

# DEFAULT_CONFIG_PATH = Path(TEST_DIR, 'tests/_test_config_inlake.cfg')

# global str_shawa
# global aiocli_kafka
# global aiocli_s3

app = FastAPI()
app.include_router(schemas.router)
app.include_router(ingress.router)

# https://gitter.im/tiangolo/fastapi?at=5ed13aea4412600ccd76f69c

@app.on_event("startup")
async def startup_event():
    # SCHEMA REGISTRY
    # Connect
    # inllogger.info("STARTUP Session configuration on startup : SchemaRegistry, [Kafka, S3]")
    # inllogger.info(f"SCHEMAREGISTRY conf : {dict(global_config['schema-registry'])}")
    # await client_schema.connect_sr(conf=dict(global_config['schema-registry']))

    # KAFKA
    # Connect
    # inllogger.info(f"KAFKA conf : {dict(global_config['kafka'])} and : {dict(global_config['kafka-aio'])}")
    # await client_kafka.connect_and_start(basicconfig=dict(global_config['kafka']), aioconfig=dict(global_config['kafka-aio']))
    inllogger.info(f"KAFKA_BOOTSTRAP_SERVER= {os.environ['KAFKA_BOOTSTRAP_SERVER']}")
    await client_kafka.connect_and_start(basicconfig={'bootstrap.servers':os.environ['KAFKA_BOOTSTRAP_SERVER']}, 
                                         aioconfig={'bootstrap_servers':os.environ['KAFKA_BOOTSTRAP_SERVER']})
    
    # Create inlake topics
    inlk_topics = [os.environ['INLAKE_TOPIC_INGRESS_UNSECURED'], os.environ['INLAKE_TOPIC_INGRESS_SECURED']]
    inllogger.info(f"INLAKE KAFKA TOPICS : {inlk_topics}")
    for topic in inlk_topics:
        if (topic in client_kafka.adminclient.list_topics(topic).topics) is False:
            client_kafka.create_topic(topic)
        else:
            inllogger.info(f"Skipping, Topic {topic} already exists")
    


@app.on_event("shutdown")
async def shutdown_event():
    await client_kafka.stop()
    # aio_producer.close()
    # producer.close()
    # s3api.close()


@app.get("/")
async def read_main():
    return {"msg": "Hello World"}

