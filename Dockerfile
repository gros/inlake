FROM python:3.9-slim-bookworm
# ENV PYTHONPATH "${PYTHONPATH}:/app/"
WORKDIR /code
COPY ./requirements.txt /code/requirements.txt
RUN pip install --upgrade pip
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY .env /code/.env
COPY ./app /code/app
COPY ./inlake /code/inlake
# COPY ./config_inlake_instack.cfg /code/config_inlake.cfg

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]