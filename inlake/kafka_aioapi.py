from aiokafka import AIOKafkaProducer
from confluent_kafka.admin import AdminClient, NewTopic

import logging
logging.basicConfig(level=logging.INFO)
    

class AIOKafkaApi():
    def __init__(self) -> None:
        self.adminclient         = None
        self._producer           = None
        self._producer_avro      = None

    # Async
    ##############################
        
    async def connect_and_start(self, basicconfig: dict, aioconfig: dict):
        """Config and start the producers
        TODO: wait for aiokafkaMAJ to have an async admin client
        
        Args:
            basicconfig (dict): config dictionnary for Kafka Admin client
            aioconfig (dict): config dictionnary for asynchronous Kafka Producers
        """
        self.adminclient         = AdminClient(basicconfig)
        self._producer           = AIOKafkaProducer(**aioconfig)
        self._producer_avro      = AIOKafkaProducer(**aioconfig, value_serializer=self.avro_serializer)
        # self._producer_protobuf  = AIOKafkaProducer(**aioconfig, value_serializer=self.protobuf_serializer)

        await self._producer.start()
        await self._producer_avro.start()
        # await self.adminclient.stop()

    async def start(self):
        await self._producer.start()
        await self._producer_avro.start()
        # await self.adminclient.stop()

    async def stop(self):
        await self._producer.stop()
        await self._producer_avro.stop()
        # await self.adminclient.stop()


    # Admin
    ##############################
        """
        from: https://www.confluent.io/blog/kafka-python-asyncio-integration/
        'Unlike the Producer, the Python AdminClient uses futures to communicate the outcome of requests back to the application 
        (though a poll loop is still required to handle log, error, and statistics events). These are not asyncio Futures though. 
        Rather, they are of type concurrent.futures. Future. In order to await them in your asyncio application, 
        you’ll need to convert them using the asyncio.wrap_future method.'

        Adminclient will be correctly integrated in next aiokafka release, wait for it instead of dev/monkeypatch
            + Current behavior and tests doesn't hold good async behaviour ;
            + Tests should be refactored.
        """


    async def healthcheck(self):
        """Healtcheck to ensure connection with a Kafka Cluster
        Actually, a simple topic listing ; which is a discount healtcheck
        TODO: external monitoring (because there should be multiple brokers so zookeeper ?) Another function ? 
 <
        Returns:
            bool: True for connection success
        """
        logging.debug(f"HEALTH CHECK ")
        if self.adminclient.list_topics() is not None:
            topi = self.adminclient.list_topics()
            logging.debug(f"HEALTHY TOPICS : {topi.topics}")
            return True
        else:
            return False

    # CRUD Topic
    ##############################
    def create_topic(self, topic_name, partitions=10, replication_factor=1):
        """Simple wrapper for Kafka Topic Creation, partitions and replication factors 
        are to be overriden following https://www.confluent.io/blog/how-choose-number-topics-partitions-kafka-cluster/

        Args:
            topic_name (str): the name of the Kafka topic to be created
            partitions (int, optional): number of partitions (parallelisme units) for the Kafka topic. Defaults to 10.
            replication_factor (int, optional): number of replications for the Kafka topic. Defaults to 1.

        Returns:
            ...: returns a future, hardly awaitable, see TODO
        """
        logging.info(f"CREATING topic {topic_name}")
        new_topic = NewTopic(topic=topic_name, num_partitions=partitions, replication_factor=replication_factor)
        return self.adminclient.create_topics([new_topic])

    def delete_topic(self, topic_name):
        """Simple wrapper for Kafka Topic Deletion
        Args:
            topic_name (str): the name of the Kafka topic to be deleted
        Returns:
            ...: returns a future, hardly awaitable, see TODO
        """
        logging.info(f"DELETING topic {topic_name}")
        # new_topic = NewTopic(topic=topic_name, num_partitions=partitions, replication_factor=replication_factor)
        return self.adminclient.create_topics([topic_name])

    # Producer
    ##############################
    async def produce_message(self, topic: str, key: bytes, value: bytes):
        """Simple wrapper for Kafka Producer
        note: override the send_and_wait to avoid the unreadable ValueError due to async or bad bytes format

        Args:
            topic (str): the Kafka topic name, aimed for message production
            key (bytes): Kafka message key
            value (bytes): Kafka message value

        Returns:
            _type_: _description_
        """
        try:
            logging.info(f"AIOProducing message {key}:{value} to topic {topic}")
            return await self._producer.send_and_wait(topic=topic, key=key, value=value)
        except BaseException as e:
            # print(e.message, e.args)
            print(e)

    async def produce_message_str(self, topic: str, key: str, value: str):
        """Simple wrapper for Kafka Producer
        note: override the send_and_wait to avoid the unreadable ValueError due to async or bad bytes format

        Args:
            topic (str): the Kafka topic name, aimed for message production
            key (str): Kafka message key, encoded in utf-8
            value (str): Kafka message value, encoded in utf-8

        Returns:
            _type_: _description_
        """
        try:
            logging.info(f"AIOProducing message {key}:{value} to topic {topic}")
            return await self._producer.send_and_wait(topic=topic, key=key.encode('utf-8'), value=value.encode('utf-8'))
        except BaseException as e:
            # print(e.message, e.args)
            print(e)

    # async def produce_message_record(self, topic: str, reco: InlkRecord):
    #     try:
    #         logging.info(f"AIOProducing message {reco.in_schema.name}:{reco.content} to topic {topic}")
    #         return await self._producer.send_and_wait(topic=topic, key=(reco.in_schema.name).encode('utf-8'), value=(reco.content).encode('utf-8'))
    #     except BaseException as e:
    #         print(e.message, e.args)



    async def produce_avro_serialized(self):
        pass

    # Serialization
    ##############################
    def avro_serializer(self, value):
        return value


    