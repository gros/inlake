import asyncio
from threading import Thread
import confluent_kafka
from confluent_kafka import KafkaException
from confluent_kafka.admin import AdminClient, NewTopic
import logging
logging.basicConfig(level=logging.INFO)


class basicKafkaAPI:
    def __init__(self, configs):
        self.adminclient    = AdminClient(conf=configs)
        self._producer      = confluent_kafka.Producer(configs)
        # self._dead_letter_queue = Redislite

    # Admin
    ##############################
    def healthcheck(self):
        """Temporary healtcheck with list topics
           TODO: external monitoring (because there should be multiple brokers so zookeeper ?) Another function ? 
 
        Returns:
            bool: True for connection success
        """
        if self.adminclient.list_topics() is not None:
            return True
        else:
            return False

    def create_topic(self, topic_name, partitions=10, replication_factor=1) -> bool:
        logging.info(f"CREATING topic {topic_name}")
        new_topic = NewTopic(topic=topic_name, num_partitions=partitions, replication_factor=replication_factor)
        return self.adminclient.create_topics([new_topic])


    # Producer
    ##############################
    def produce_message(self, topic, key, value):
        try:
            self._producer.produce(topic=topic, key=key, value=value, callback=self.delivery_callback)

        except BufferError as e:
            logging.error('%% Local producer queue is full (%d messages awaiting delivery): try again\n' %
                             len(self._producer))

        # NOTE From https://github.com/confluentinc/confluent-kafka-python/blob/master/examples/producer.py
        # Serve delivery callback queue.
        # note: Since produce() is an asynchronous API this poll() call
        #       will most likely not serve the delivery callback for the
        #       last produce()d message.
        self._producer.poll(0)

    def produce_avro_serialized(self):
        pass

    
    def delivery_callback(err, msg):
        if err:
            logging.error('%% Message failed delivery: %s\n' % err)
        else:
            logging.info('%% Message delivered to %s [%d] @ %d\n' %
                             (msg.topic(), msg.partition(), msg.offset()))

    # Dead Letter Management 
    ##############################
    def _post_to_dead_letter_queue(self):
        pass
    
    def _flush_dead_letter_queue(self):
        pass
    