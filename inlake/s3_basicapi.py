import os
import sys
import logging
import uuid
from datetime import timedelta

import requests
import configparser

# # from minio import Minio
# from boto3 import Session
# from botocore.client import Config
# from botocore.exceptions import ClientError

from aiobotocore.session import get_session

import asyncio
import threading
from threading import Thread

class AIOs3Api:
    def __init__(self) -> None:
        self.session         = None
        self.client          = None
    
    async def connect(self, config_dict):
        self.session         = get_session()
        cli = await self.session.create_client(
                                                's3',
                                                aws_access_key_id=config_dict['aws_access_key_id'],
                                                aws_secret_access_key=config_dict['aws_secret_access_key'],
                                            )
        self.client          = cli

    async def aio_listbuckets(self):
        # data = b'\x01' * 1024
        # resp = await client.put_object(Bucket=bucket, Key=key, Body=data)
        resp = await self.client.list_buckets()
        print(resp)

    async def stop(self):
        await self.client.close()


# class basics3Api:
#     def __init__(self, config_dict, loop=None):
#         self.session = Session(aws_access_key_id=config_dict['user.access.key'],
#                             aws_secret_access_key=config_dict['passw.secret.key'])
        
#         self.s3client = self.session.client(
#             "s3",
#             endpoint_url=config_dict['s3.endpoint'],
#             verify=False,
#             config=Config(signature_version="s3v4", s3={"addressing_style": "path"}))
        
#         self.s3resource = self.session.resource('s3',
#                     endpoint_url=config_dict['s3.endpoint'],
#                     aws_access_key_id=config_dict['user.access.key'],
#                     aws_secret_access_key=config_dict['passw.secret.key'])
    

#     ########################
#     # Get- Objects
#     ########################
#     async def aio_get_object_attributes(self, bucket_name, content_key):
#         result = self._loop.create_future() # TODO: à approfondir, rien n'est awaité   
#         return self.s3client.get_object_attributes(Bucket=bucket_name, Key=content_key, ObjectAttributes=['ObjectSize'])


#     ########################
#     # Get- Lists
#     ########################
#     async def aio_list_all(self):
#         result = self._loop.create_future() # TODO: à approfondir, rien n'est awaité  
#         out_list = []
#         for bucket in self.s3resource.buckets.all():
#             out_list.append(bucket.name)
#             for obj in bucket.objects.all():
#                 out_list.append(f"({bucket.name}) name: {obj.key}, size:{obj.size}")
#         return out_list

#     async def aio_list_buckets(self):
#         result = self._loop.create_future() # TODO: à approfondir, rien n'est awaité     
#         return self.s3client.list_buckets()
    
#     async def aio_list_bucketobjects(self, bucket_name):
#         return self.s3client.list_objects(Bucket=bucket_name)

#     ########################
#     # Put - Object
#     ########################
#     async def aio_put_object(self, data, content_key, bucket_name, object_name=None):
#         result = self._loop.create_future() # TODO: à approfondir, rien n'est awaité  
#         try:
#             self.s3client.put_object(Body=data, Bucket=bucket_name, Key=content_key)
#             # self.s3resource.Bucket(bucket).put_object(Key=file_name, Body=data)
#             # response = self.s3client.upload_file(   file_name, 
#             #                                         bucket,
#             #                                         object_name,
#             #                                         ExtraArgs={'Metadata': {'metakey': 'metamyvalue'}}
#             #                                         # Callback=ProgressPercentage('FILE_NAME')
#             #                                         )
#         except ClientError as e:
#             logging.error(e)
#             return False
#         return True
    
#     ########################
#     # UTILS
#     ########################
#     async def healthcheck_s3(self) -> bool:
#         pass